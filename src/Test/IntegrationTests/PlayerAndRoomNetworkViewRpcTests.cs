﻿using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNet.Structs;
using PNetR;
using Room = PNetR.Room;
using Server = PNetS.Server;

namespace IntegrationTests
{
    [TestClass]
    public class PlayerAndRoomNetworkViewRpcTests
    {
        private Server _dispatcher;
        private TestRoom _room;
        private TestClient _client;
        private PNetS.Player _dPlayer;
        private PNetR.Player _rPlayer;
        private PNetS.Room _dRoom;

        private static TestContext _context;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _context = context;
            Integration.SetupLoggers();
        }

        [TestInitialize]
        public void TestSetup()
        {
            _dispatcher = Integration.GetReadyServer();
            _room = Integration.GetConnectedRoom(_dispatcher);
            if (!_dispatcher.TryGetRoom(_room.Room.RoomId, out _dRoom))
                Assert.Fail("Could not get dispatcher room");
            _client = Integration.GetConnectedClient(_dispatcher);
            _dPlayer = _dispatcher.GetPlayer(_client.Client.PlayerId);
            _dPlayer.ChangeRoom(_dRoom);
            Integration.WaitUntil(() => _client.Client.Room.RoomId == _room.Room.RoomId);
            Integration.WaitUntil(() => _room.Room.GetPlayer(_client.Client.PlayerId) != null);
            _rPlayer = _room.Room.GetPlayer(_client.Client.PlayerId);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _dispatcher.Shutdown();
            _room.Cleanup();
            _client.Cleanup();
            _dispatcher = null;
            _client = null;
            _room = null;
        }

        [TestMethod]
        public void RpcTest()
        {
            PNetC.NetworkView cview = null;
            PNetR.NetworkView rview = null;
            _client.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
// ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    cview = instance;
            };

            var player = _room.Room.GetPlayer(_dPlayer.Id);
            Assert.IsNotNull(player);

            rview = _room.Room.Instantiate("foo", new Vector3F(10, 10, 8), new Vector3F());

            Integration.WaitUntil(() => cview != null);

            var clientReceived = false;
            cview.SubscribeToRpc(1, 8, (msg) =>
            {
                var recv = msg.ReadInt32();
                Assert.AreEqual(TestRComp.Rpc8Val, recv);
                clientReceived = true;
            });

            rview.Rpc<TestRComp>(8, player, TestRComp.Rpc8Val);

            Integration.WaitUntil(() => clientReceived);

            var serverReceived = false;
            rview.SubscribeToRpc(1, 27, (msg, info) =>
            {
                var recv = msg.ReadString();
                Assert.AreEqual("Hello server", recv);
                Assert.AreEqual(player, info.Sender);
                serverReceived = true;
            });

            cview.Rpc<TestRComp>(27, RpcMode.ServerOrdered, "Hello server");

            Integration.WaitUntil(() => serverReceived);
        }

        [TestMethod]
        public void DelayedConnectSpawnTest()
        {
            var c2 = Integration.GetClient();

            PNetC.NetworkView cview = null;
            PNetC.NetworkView c2view = null;
            PNetR.NetworkView rview = null;

            _client.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
                // ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    cview = instance;
            };

            c2.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
                // ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    c2view = instance;
            };

            rview = _room.Room.Instantiate("foo", new Vector3F(10, 10, 8), new Vector3F());

            Integration.WaitUntil(() => cview != null);

            var d2 = c2.Connect(_dispatcher);
            d2.ChangeRoom(_dRoom);
            Integration.WaitUntil(() => c2.Client.Room.RoomId == _room.Room.RoomId);

            Integration.WaitUntil(() => c2view != null);
        }

        [TestMethod]
        public void PlayerToPlayerTest()
        {
            var c2 = Integration.GetConnectedClient(_dispatcher);

            var d2 = _dispatcher.GetPlayer(c2.Client.PlayerId);
            d2.ChangeRoom(_dRoom);
            Integration.WaitUntil(() => c2.Client.Room.RoomId == _room.Room.RoomId);

            PNetC.NetworkView cview = null;
            PNetC.NetworkView c2view = null;
            PNetR.NetworkView rview = null;

            _client.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
                // ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    cview = instance;
            };

            c2.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
                // ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    c2view = instance;
            };

            rview = _room.Room.Instantiate("foo", new Vector3F(10, 10, 8), new Vector3F());
            var rReceived = false;
            rview.SubscribeToRpc(1, 8, (message, info) =>
            {
                rReceived = true;
            });

            Integration.WaitUntil(() => cview != null, 10);
            Integration.WaitUntil(() => c2view != null, 10);

            var clientReceived = false;
            cview.SubscribeToRpc(1, 8, (msg) =>
            {
                var recv = msg.ReadInt32();
                Assert.AreEqual(TestRComp.Rpc8Val, recv);
                clientReceived = true;
            });
            
            var c2Received = false;
            c2view.SubscribeToRpc(1, 8, msg =>
            {
                var recv = msg.ReadInt32();
                Assert.AreEqual(TestRComp.Rpc8Val, recv);
                c2Received = true;
            });

            cview.Rpc<TestRComp>(8, RpcMode.OthersOrdered, TestRComp.Rpc8Val);

            Integration.WaitUntil(() => c2Received, 5);

            c2view.Rpc<TestRComp>(8, RpcMode.AllOrdered, TestRComp.Rpc8Val);

            Integration.WaitUntil(() => clientReceived);

            Assert.IsTrue(rReceived);
        }

        #region proxy tests
        public class RProxyRecieve : IServerProxyComponent
        {
            public bool WasCalled = false;
            public void DoSomething(int val, TestEnum enu)
            {
                Assert.AreEqual(135, val);
                Assert.AreEqual(TestEnum.EleventySeven, enu);
                WasCalled = true;
            }

            [Rpc(15)]
            public int ReturnRpc(int val)
            {
                return val*2;
            }
        }

        class RProxySend : IClientProxyComponent, INetComponentProxy
        {
            public void DoSomethingClient(string val, IntSerializer ser)
            {
                if (CurrentSendTo == null)
                    NetworkView.Rpc<IClientProxyComponent>(7, CurrentRpcMode, val, ser);
                else
                    NetworkView.Rpc<IClientProxyComponent>(7, CurrentSendTo, val, ser);
            }

            public RpcMode CurrentRpcMode { get; set; }
            public Player CurrentSendTo { get; set; }
            public NetworkView NetworkView { get; set; }
        }

        public class CProxyReceive : IClientProxyComponent
        {
            public bool WasCalled = false;
            public void DoSomethingClient(string val, IntSerializer ser)
            {
                Assert.AreEqual("client test", val);
                Assert.AreEqual(1235577, ser.Value);
                WasCalled = true;
            }
        }

        class CProxySend : IServerProxyComponent, PNetC.INetComponentProxy
        {
            public void DoSomething(int val, TestEnum enu)
            {
                NetworkView.Rpc<IServerProxyComponent>(10, CurrentRpcMode, val, enu);
            }

            public RpcMode CurrentRpcMode { get; set; }
            public PNetC.NetworkView NetworkView { get; set; }
        }

        public enum TestEnum : ushort
        {
            Zero,
            One,
            Two,
            EleventySeven = 117,
        }

        [NetComponent(5)]
        interface IServerProxyComponent
        {
            [Rpc(10)]
            void DoSomething(int val, TestEnum enu);
        }

        [NetComponent(5)]
        interface IClientProxyComponent
        {
            [Rpc(7)]
            void DoSomethingClient(string val, IntSerializer ser);
        }

        [TestMethod]
        public void ProxyTest()
        {
            NetworkView rview = null;
            PNetC.NetworkView cview = null;

            _client.Client.NetworkManager.ViewInstantiated += (instance, position, rotation) =>
            {
                // ReSharper disable once AccessToModifiedClosure
                Assert.IsNotNull(rview);
                if (instance.Id == rview.Id)
                    cview = instance;
            };

            rview = _room.Room.Instantiate("foo", new Vector3F(10, 10, 7), new Vector3F());
            cview = _client.Client.NetworkManager.Get(rview.Id);

            Integration.WaitUntil(() => cview != null);

            var rrecv = new RProxyRecieve();
            var rsend = new RProxySend();
            rview.SubscribeMarkedRpcsOnComponent(rrecv);
            rview.AddProxy(rsend);

            var crecv = new CProxyReceive();
            var csend = new CProxySend();
            cview.SubscribeMarkedRpcsOnComponent(crecv);
            cview.AddProxy(csend);

            rview.Proxy<IClientProxyComponent>().DoSomethingClient("client test", new IntSerializer(1235577));

            Integration.WaitUntil(() => crecv.WasCalled);

            cview.Proxy<IServerProxyComponent>().DoSomething(135, TestEnum.EleventySeven);

            Integration.WaitUntil(() => rrecv.WasCalled);

            bool gotRet = false;
            cview.RpcContinueWith<int>(5, 15, RpcMode.ServerOrdered, 12).Complete((ret) =>
            {
                Assert.AreEqual(24, ret);
                gotRet = true;
            });
            Integration.WaitUntil(() => gotRet, 60);
        }
        #endregion

        [TestMethod]
        public void InstantiationFinishedTest()
        {
            var rview = _room.Room.Instantiate("foo", new Vector3F(10, 10, 8), new Vector3F());
            bool rInst = false;
            rview.FinishedInstantiation += player =>
            {
                Assert.AreSame(_rPlayer, player);
                rInst = true;
            };

            Integration.WaitUntil(() => _client.Client.NetworkManager.Get(rview.Id) != null);
            Integration.WaitUntil(() => rInst);
        }

        [TestMethod]
        public void DestroyTest()
        {
            var rview = _room.Room.Instantiate("foo", new Vector3F(), new Vector3F());

            var id = rview.Id;
            Integration.WaitUntil(() => _client.Client.NetworkManager.Get(id) != null);
            var cview = _client.Client.NetworkManager.Get(rview.Id);

            bool didDestroy = false;
            cview.Destroyed += b =>
            {
                Assert.AreEqual(3, b);
                didDestroy = true;
            };

            Assert.IsTrue(_room.Room.Destroy(rview, 3));

            Integration.WaitUntil(() => didDestroy);
            Integration.WaitUntil(() => _client.Client.NetworkManager.Get(id) == null);
            Integration.WaitUntil(() => _room.Room.NetworkManager.Get(id) == null);
        }
    }

    [NetComponent(1)]
    public class TestRComp
    {
        public const int Rpc8Val = 3;

        [Rpc(8)]
        void Rpc(NetMessage message, NetMessageInfo info)
        {
            var val = message.ReadInt32();
            Assert.Equals(Rpc8Val, val);
        }
    }

    [NetComponent(2)]
    public class TestCComp
    {
        public const int Rpc8Val = 3;
    }
}
