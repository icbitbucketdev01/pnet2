using System;
using System.Threading;
using PNet;
using PNetR;

namespace IntegrationTests
{
    internal class TestRoom
    {
        public readonly Room Room;
        public TestRoom(Room room)
        {
            Room = room;
            var runThread = new Thread(Loop);
            runThread.Start();
        }

        private bool quit = false;
        private void Loop()
        {
            while (!quit)
            {
                Room.ReadQueue();
                Thread.Sleep(1);
            }
        }

        public void Cleanup()
        {
            Room.Shutdown();

            for (int j = 0; j < 1000; j++)
            {
                if (ConnectionStatus.Disconnected == Room.ServerStatus)
                    break;
                Thread.Sleep(1);
            }
        }
    }
}