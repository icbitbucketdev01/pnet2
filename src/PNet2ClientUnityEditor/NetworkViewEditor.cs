﻿using UnityEditor;
using UnityEngine;

namespace PNetU.Editor
{
    [CustomEditor(typeof(NetworkView))]
    class NetworkViewEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var view = target as NetworkView;
            if (view == null) return;

            GUILayout.Label("ID: " + view.ViewId.Id);
            GUILayout.Label("Owner: " + view.OwnerId);
            GUILayout.Label("IsMine: " + view.IsMine);
        }
    }
}