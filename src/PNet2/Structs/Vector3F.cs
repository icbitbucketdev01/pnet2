﻿using System.Globalization;

namespace PNet.Structs
{
    public struct Vector3F : INetSerializable
    {
        private float _x;
        private float _y;
        private float _z;

        public float X
        {
            get { return _x; }
        }

        public float Y
        {
            get { return _y; }
        }

        public float Z
        {
            get { return _z; }
        }

        public Vector3F(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public Vector3F(NetMessage msg) : this()
        {
            OnDeserialize(msg);
        }

        /// <summary>
        /// write to the message
        /// </summary>
        /// <param name="message">message to write to</param>
        public void OnSerialize(NetMessage message)
        {
            message.Write(_x);
            message.Write(_y);
            message.Write(_z);
        }

        /// <summary>
        /// read the message
        /// </summary>
        /// <param name="message">message to read from</param>
        public void OnDeserialize(NetMessage message)
        {
            _x = message.ReadFloat();
            _y = message.ReadFloat();
            _z = message.ReadFloat();
        }

        /// <summary>
        /// size in bytes
        /// </summary>
        public int AllocSize
        {
            get { return 12; }
        }

        /// <summary>
        /// invariant culture
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}",
                X.ToString(CultureInfo.InvariantCulture),
                Y.ToString(CultureInfo.InvariantCulture),
                Z.ToString(CultureInfo.InvariantCulture));
        }
    }
}
