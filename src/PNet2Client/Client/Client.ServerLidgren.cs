﻿#if S_LIDGREN
using System.Net;
using PNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace PNetC
{
    public partial class Client
    {
        internal NetClient ServerClient { get; private set; }
        private NetPeerConfiguration _serverConfiguration;

        partial void ImplDispatchStart()
        {
            _serverConfiguration = new NetPeerConfiguration(Configuration.AppIdentifier);

#if DEBUG
            Debug.Log("Debug build. Simulated latency and packet loss/duplication is enabled.");
            _serverConfiguration.SimulatedLoss = 0.001f;
            _serverConfiguration.SimulatedDuplicatesChance = 0.001f;
            _serverConfiguration.SimulatedMinimumLatency = 0.1f;
            _serverConfiguration.SimulatedRandomLatency = 0.01f;
#endif

            ServerClient = new NetClient(_serverConfiguration);
            ServerClient.Start();

            NetOutgoingMessage hailMsg = null;
            if (HailObject != null)
            {
                var size = Serializer.SizeOf(HailObject);
                var msg = NetMessage.GetMessage(size);
                Serializer.Serialize(HailObject, msg);

                hailMsg = ServerClient.CreateMessage(size);
                msg.Clone(hailMsg);
            }
            ServerClient.Connect(Configuration.ServerAddress, Configuration.ServerPort, hailMsg);
        }

        private bool _serverShutdown;
        partial void ImplDispatchDisconnect(string reason)
        {
            ServerClient.Shutdown(reason);
        }

        private int _lastFrameSize = 16;
        private int _lastServerSize = 16;
        partial void ImplDispatchRead()
        {
            Time = NetTime.Now;
            ReadServerMessages();
        }

        void ReadServerMessages()
        {
            if (ServerClient == null) return;
            var messages = new List<NetIncomingMessage>(_lastServerSize * 2);
            _lastServerSize = ServerClient.ReadMessages(messages);

            if (_serverShutdown)
            {
                if (ServerClient.Status == NetPeerStatus.NotRunning)
                {
                    _serverShutdown = false;
                    FinalizeDisconnect();
                }
                else if (ServerClient.Status == NetPeerStatus.Running)
                {
                    ServerClient.Shutdown(Server.StatusReason);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < messages.Count; i++)
            {
                var msgType = messages[i].MessageType;
                var msg = NetMessage.GetMessage(messages[i].Data.Length);
                messages[i].Clone(msg);
                msg.Sender = messages[i].SenderConnection;
                ServerClient.Recycle(messages[i]);

                if (msgType == NetIncomingMessageType.Data)
                {
                    Server.ConsumeData(msg);
                }
                else if (msgType == NetIncomingMessageType.DebugMessage)
                {
                    Debug.Log(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.WarningMessage)
                {
                    Debug.LogWarning(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.StatusChanged)
                {
                    var status = (NetConnectionStatus)msg.ReadByte();
                    var statusReason = msg.ReadString();
                    if (status == NetConnectionStatus.Connected)
                    {
                        var serverConn = ServerClient.ServerConnection;
                        if (serverConn == null)
                            throw new NullReferenceException("Could not get server connection after connected");
                        var remsg = serverConn.RemoteHailMessage;
                        if (remsg == null)
                            throw new NullReferenceException("Could not get player id");
                        if (remsg.RemainingBits < 16)
                            throw new Exception("Could not read player id");
                        var id = remsg.ReadUInt16();
                        PlayerId = id;
                        Server.Connection = serverConn ;
                        Debug.Log("Connected to server as {0}", id);
                    }
                    else if (status == NetConnectionStatus.Disconnected)
                    {
                        Server.Connection = null;
                        PlayerId = 0;
                    }
                    Debug.Log("Server Status: {0}, {1}", status, statusReason);
                    var lastStatus = Server.Status;
                    Server.Status = status.ToPNet();
                    Server.StatusReason = statusReason;

                    try
                    {
                        if (Server.Status == ConnectionStatus.Disconnected)
                        {
                            switch (lastStatus)
                            {
                                case ConnectionStatus.Disconnecting:
                                case ConnectionStatus.Connected:
                                    _serverShutdown = true;
                                    break;
                                default:
                                    if (OnFailedToConnect != null)
                                        OnFailedToConnect(statusReason);
                                    break;
                            }
                            
                        }
                        else if (Server.Status == ConnectionStatus.Connected)
                        {
                            if (OnConnectedToServer != null)
                                OnConnectedToServer();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e, "[Net.Update.StatusChanged]");
                    }
                }
                else if (msgType == NetIncomingMessageType.Error)
                {
                    Debug.LogException(new Exception(msg.ReadString()){Source = "Client.Lidgren.ReadServerMessages [Lidgren Error]"}); //this should really never happen...
                }
                else
                {
                    Debug.LogWarning("Uknown message type {0} from server", msgType);
                }

                NetMessage.RecycleMessage(msg);
            }
        }

        internal void ServerDisconnect()
        {
            ServerClient.Disconnect("unequaldisconnect");
        }
    }
}

#endif