﻿#if R_LIDGREN
using System.Net;
using PNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace PNetC
{
    partial class Client
    {
        internal NetClient RoomClient { get; private set; }
        private NetPeerConfiguration _roomConfiguration;

        partial void ImplRoomStart()
        {
            _roomConfiguration = new NetPeerConfiguration(Configuration.AppIdentifier);

            RoomClient = new NetClient(_roomConfiguration);
            RoomClient.Start();

#if DEBUG
            _roomConfiguration.SimulatedLoss = 0.001f;
            _roomConfiguration.SimulatedDuplicatesChance = 0.001f;
            _roomConfiguration.SimulatedMinimumLatency = 0.1f;
            _roomConfiguration.SimulatedRandomLatency = 0.01f;
#endif
        }

        partial void ImplementationSwitchRoom(IPEndPoint endPoint)
        {
            _nendPoint = endPoint;
            if (RoomClient.ConnectionStatus != NetConnectionStatus.Disconnected)
            {
#if DEBUG
                Debug.Log("begin room switch waiting for disconnect");
#endif
                _switchState = waitForSwitchState.WaitForDc;
                RoomClient.Disconnect(PtoDMsgs.RoomSwitch);
            }
            else
            {
#if DEBUG
                Debug.Log("begin room switch waiting for room switch");
#endif
                WaitForRoomSwitch();
            }

            if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
                NetworkManager.DestroyAll();
        }

        void WaitForRoomSwitch()
        {
            _switchState = waitForSwitchState.WaitForRoomSwitch;
            //and inform users that we're switching rooms.
            try
            {
                BeginRoomSwitch.Raise(_queuedSwitchRoomId);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        partial void ImplRoomRead()
        {
            ReadRoomMessages();
        }

        private int _lastRoomFrameSize;
        void ReadRoomMessages()
        {
            if (RoomClient == null) return;

            var messages = new List<NetIncomingMessage>(_lastRoomFrameSize * 2);
            _lastRoomFrameSize = RoomClient.ReadMessages(messages);

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < messages.Count; i++)
            {
                var msg = NetMessage.GetMessage(messages[i].Data.Length);
                var msgType = messages[i].MessageType;
                var method = messages[i].DeliveryMethod;
                var senderConnection = messages[i].SenderConnection;
                messages[i].Clone(msg);
                msg.Sender = senderConnection;
                RoomClient.Recycle(messages[i]);

                if (msgType == NetIncomingMessageType.Data)
                {
                    Room.ConsumeData(msg);
                }
                else if (msgType == NetIncomingMessageType.DebugMessage)
                {
                    Debug.Log(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.WarningMessage)
                {
                    Debug.LogWarning(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.ConnectionLatencyUpdated)
                {
                    Room.Latency = msg.ReadFloat();
                }
                else if (msgType == NetIncomingMessageType.ErrorMessage)
                {
                    Debug.LogError(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.StatusChanged)
                {
                    var status = (NetConnectionStatus)msg.ReadByte();
                    var statusReason = msg.ReadString();
                    var lastStatus = Room.Status;
                    Room.Status = status.ToPNet();
                    Room.StatusReason = statusReason;
                    Debug.Log("Room Status: {0}, {1}", status, statusReason);

                    if (status == NetConnectionStatus.Connected)
                    {
                        var serverConn = RoomClient.ServerConnection;
                        if (serverConn == null)
                            throw new NullReferenceException("Could not get room connection after connected");
                        var remsg = serverConn.RemoteHailMessage;
                        if (remsg == null)
                            throw new NullReferenceException("Could not get room guid");
                        byte[] gid;
                        if (!remsg.ReadBytes(16, out gid))
                            throw new Exception("Could not read room guid");
                        Room.RoomId = new Guid(gid);

                        if (_switchState == waitForSwitchState.Connecting)
                        {
#if DEBUG
                            Debug.Log("finished connecting to room");
#endif
                            _switchState = waitForSwitchState.None;
                            OnFinishedRoomSwitch.Raise();
                        }
                    }

                    if (_switchState == waitForSwitchState.WaitForDc && status == NetConnectionStatus.Disconnected)
                    {
                        //fully disconnected. now we can tell users that they should switch rooms.
                        WaitForRoomSwitch();
                    }

                    if (Room.Status == ConnectionStatus.Disconnected && !_shuttingDown)
                    {
                        if (_switchState == waitForSwitchState.Connecting)
                        {
                            try
                            {
                                Debug.LogError("Failed to switch to room at {0}: {1}", _nendPoint, statusReason);
                                OnFailedRoomSwitch.Raise(statusReason);
                            }
                            catch (Exception e)
                            {
                                Debug.LogException(e);
                            }
                        }
                        else
                        {
                            switch (statusReason)
                            {
                                case PtoDMsgs.RoomSwitch:
                                    //no worry
                                    break;
                                case DtoPMsgs.BadToken:
                                case DtoPMsgs.NoRoom:
                                case DtoPMsgs.TokenTimeout:
                                case "Connection timed out":
                                default:
                                    //todo: timeout?
                                    try
                                    {
                                        OnDisconnectedFromRoom.Raise(statusReason);
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.LogException(e);
                                    }
                                    break;
                            }
                        }
                    }

                }
                else if (msgType == NetIncomingMessageType.Error)
                {
                    Debug.LogException(new Exception(msg.ReadString()) { Source = "Client.Lidgren.ReadRoomMessages [Lidgren Error]" }); //this should really never happen...
                }
                else
                {
                    Debug.LogWarning("Uknown message type {0}", msgType);
                }

                NetMessage.RecycleMessage(msg);
            }
        }

        private IPEndPoint _nendPoint;

        partial void ImplWaitForReconnect()
        {
#if DEBUG
                Debug.Log("Waiting for reconnect");
#endif
            _switchState = waitForSwitchState.Connecting;
            var hail = RoomClient.CreateMessage(16);
            hail.Write(_switchToken.ToByteArray());
            RoomClient.Connect(_nendPoint, hail);
        }

        partial void ImplRoomDisconnect(string reason)
        {
            RoomClient.Shutdown(reason);
        }
    }
}
#endif