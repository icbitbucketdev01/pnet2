﻿using System;
using System.Collections.Generic;
using System.Net;
using PNet;

namespace PNetC
{
    public partial class Client
    {
        public NetworkConfiguration Configuration { get; private set; }

        public readonly SerializationManager Serializer = new SerializationManager();

        private Guid _switchToken;
        public event Action<string> BeginRoomSwitch;
        public event Action<string> OnFailedToConnect;
        public event Action OnConnectedToServer;
        public event Action OnDisconnectedFromServer;
        public event Action<string> OnFailedRoomSwitch;
        public event Action OnFinishedRoomSwitch;
        /// <summary>
        /// this is only fired for errors/timeouts, not for room switching
        /// </summary>
        public event Action<string> OnDisconnectedFromRoom;

        public Server Server { get; private set; }
        public Room Room { get; private set; }
        /// <summary>
        /// the object to serialize to the server upon connecting, to use for connection approval.
        /// </summary>
        public object HailObject { get; set; }

        /// <summary>
        /// Player id. 0 is unset.
        /// </summary>
        public ushort PlayerId { get; private set; }

        public NetworkViewManager NetworkManager { get; private set; }
        public double Time { get; set; }

        public Client()
        {
            Server = new Server(this);
            Room = new Room(this);
            NetworkManager = new NetworkViewManager(this);
            NetComponentHelper.FindNetComponents();
        }

        /// <summary>
        /// start the server's networking.
        /// </summary>
        /// <param name="configuration"></param>
        public void StartConnection(NetworkConfiguration configuration)
        {
            Configuration = configuration;
            ImplDispatchStart();
            ImplRoomStart();
        }
        partial void ImplDispatchStart();
        partial void ImplRoomStart();

        private bool _shuttingDown;
        public void Shutdown(string reason = "Shutting down")
        {
            _shuttingDown = true;
            ImplDispatchDisconnect(reason);
            ImplRoomDisconnect(reason);
        }

        partial void ImplDispatchDisconnect(string reason);
        partial void ImplRoomDisconnect(string reason);

        public void ReadQueue()
        {
            ImplDispatchRead();
            ImplRoomRead();
        }
        partial void ImplDispatchRead();
        partial void ImplRoomRead();

        internal void OnBeginRoomSwitch(NetMessage msg)
        {
            var address = msg.ReadIPEndPoint();
            _queuedSwitchRoomId = msg.ReadString();
            _switchToken = msg.ReadGuid();

            //switch the connection
            ImplementationSwitchRoom(address);
        }
        partial void ImplementationSwitchRoom(IPEndPoint endPoint);
        partial void ImplWaitForReconnect();

        public void FinishRoomSwitch()
        {
            if (_switchState != waitForSwitchState.WaitForRoomSwitch)
                return;

            ImplWaitForReconnect();
            
            var nmsg = Server.GetMessage(2);
            nmsg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            nmsg.Write(RandPRpcs.FinishedRoomSwitch);
            Server.SendMessage(nmsg, ReliabilityMode.Ordered);
        }
        private string _queuedSwitchRoomId;

        public void Cleanup()
        {
            HailObject = null;
            BeginRoomSwitch = null;
            OnConnectedToServer = null;
            OnDisconnectedFromServer = null;
            OnDisconnectedFromRoom = null;
            OnFailedToConnect = null;
            OnFailedRoomSwitch = null;
        }

        enum waitForSwitchState
        {
            None,
            WaitForDc,
            WaitForRoomSwitch,
            Connecting,
        }

        private waitForSwitchState _switchState;

        void FinalizeDisconnect()
        {
            if (OnDisconnectedFromServer != null)
                OnDisconnectedFromServer();

            if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
            {
                NetworkManager.DestroyAll();
            }
        }
    }
}
