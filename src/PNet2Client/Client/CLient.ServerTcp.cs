﻿using System.IO;
using System.Threading.Tasks;
#if S_TCP
using System.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;

namespace PNetC
{
    partial class Client
    {
        private TcpClient _serverClient;

        internal void ServerDisconnect()
        {
            _serverClient.Close();
        }

        partial void ImplDispatchStart()
        {
            _serverClient = new TcpClient();
            Server.Status = ConnectionStatus.Connecting;
            _serverClient.BeginConnect(Configuration.ServerAddress, Configuration.ServerPort, ConnectCallback, _serverClient);
        }

        private async void ConnectCallback(IAsyncResult ar)
        {
            var client = ar.AsyncState as TcpClient;
            try
            {
                if (client != _serverClient)
                {
                    throw new Exception("TcpClient Mismatch");
                }

                client.EndConnect(ar);
                Server.StatusReason = "Connection established, waiting for auth";
                Debug.Log("Authenticating with server");

                using (var networkStream = client.GetStream())
                {
                    //send auth...
                    if (HailObject != null)
                    {
                        var size = Serializer.SizeOf(HailObject);
                        var msg = Server.GetMessage(size);
                        Serializer.Serialize(HailObject, msg);
                        msg.WriteSize();

                        await networkStream.WriteAsync(msg.Data, 0, msg.LengthBytes);
                    }

                    var buffer = new byte[1024];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;


                    var authMessages = new Queue<NetMessage>();
                    while (!_shuttingDown)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, authMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    if (authMessages.Count == 0)
                        throw new Exception("Could not read player id");
                    var authMsg = authMessages.Dequeue();

                    var auth = authMsg.ReadBoolean();
                    authMsg.ReadPadBits();
                    if (!auth)
                    {
                        string reason;
                        if (!authMsg.ReadString(out reason))
                            reason = "Not authorized";
                        OnFailedToConnect.TryRaise(reason, Debug.Logger);
                        return;
                    }
                    if (authMsg.RemainingBits < 16)
                        throw new Exception("Could not read player id");
                    var id = authMsg.ReadUInt16();
                    PlayerId = id;
                    Server.NetworkStream = networkStream;
                    Server.TcpClient = client;
                    Server.Status = ConnectionStatus.Connected;
                    Server.StatusReason = "Connected";

                    Debug.Log("Connected to server");
                    //and drain the rest of messages that might have come after the auth
                    while (authMessages.Count > 0)
                        EnqueueMessage(authMessages.Dequeue());

                    while (!_shuttingDown)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, EnqueueMessage);
                        }
                        if (!client.Connected)
                            return;
                    }

                }
            }
            catch (ObjectDisposedException ode)
            {
                if (!_shuttingDown && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
                Server.Status = ConnectionStatus.Disconnected;
                Server.NetworkStream = null;
                OnDisconnectedFromServer.TryRaise(Debug.Logger);
            }
        }

        private readonly Queue<NetMessage> _incomingMessages = new Queue<NetMessage>();
        private void EnqueueMessage(NetMessage msg)
        {
            lock(_incomingMessages)
                _incomingMessages.Enqueue(msg);
        }

        partial void ImplDispatchDisconnect(string reason)
        {
            var shutReason = Server.GetMessage(reason.Length * 2 + 6);
            shutReason.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            shutReason.Write(DandPRpcs.DisconnectMessage);
            shutReason.Write(reason);
            Server.SendMessage(shutReason, ReliabilityMode.Ordered);
            _serverClient.Close();
        }

        partial void ImplDispatchRead()
        {
            while (true)
            {
                NetMessage msg;
                lock (_incomingMessages)
                {
                    if (_incomingMessages.Count > 0)
                        msg = _incomingMessages.Dequeue();
                    else
                        return;
                }
                Server.ConsumeData(msg);
                NetMessage.RecycleMessage(msg);
            }
        }
    }
}
#endif