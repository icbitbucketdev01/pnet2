﻿#if R_LIDGREN
using PNet;

namespace PNetC
{
    partial class NetworkedSceneView
    {
        partial void ImplSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _net.RoomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            if (seq == 2) seq = 3; //don't use player channel...

            _net.RoomClient.SendMessage(lmsg, method, seq);
        }
    }
}
#endif