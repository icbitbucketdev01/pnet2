﻿using System;
using System.Collections.Generic;
using System.Text;
using PNet;

namespace PNetC
{
    /// <summary>
    /// Objects that exist in a scene with pre-synchronized network id's
    /// </summary>
    public partial class NetworkedSceneView
    {
        ushort _networkID;
        private readonly Client _net;

        /// <summary>
        /// The scene/room Network ID of this item. Should be unique per object
        /// </summary>
        public ushort NetworkID
        {
            get
            {
                return _networkID;
            }
            set { _networkID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="networkID"></param>
        /// <param name="net"></param>
        public NetworkedSceneView(ushort networkID, Client net)
        {
            _networkID = networkID;
            _net = net;
            SceneObjects[_networkID] = this;
        }

        /// <summary>
        /// only used for serialization if you really need to. Does not actually make it accessible to the network
        /// </summary>
        /// <param name="networkID"></param>
        public NetworkedSceneView(ushort networkID)
        {
            _networkID = networkID;
        }
        
        /// <summary>
        /// Should be called by implementing engine upon a scene change, if relevent
        /// </summary>
        public static void ClearSceneIDs()
        {
            SceneObjects.Clear();
        }

        #region RPC Processing

        readonly Dictionary<byte, Action<NetMessage>> _rpcProcessors = new Dictionary<byte, Action<NetMessage>>();

        /// <summary>
        /// Subscribe to an rpc
        /// </summary>
        /// <param name="rpcID">id of the rpc</param>
        /// <param name="rpcProcessor">action to process the rpc with</param>
        /// <param name="overwriteExisting">overwrite the existing processor if one exists.</param>
        /// <returns>Whether or not the rpc was subscribed to. Will return false if an existing rpc was attempted to be subscribed to, and overwriteexisting was set to false</returns>
        public bool SubscribeToRpc(byte rpcID, Action<NetMessage> rpcProcessor, bool overwriteExisting = true)
        {
            if (rpcProcessor == null)
                throw new ArgumentNullException("rpcProcessor", "the processor delegate cannot be null");
            if (overwriteExisting)
            {
                _rpcProcessors[rpcID] = rpcProcessor;
                return true;
            }
            
            Action<NetMessage> checkExist;
            if (_rpcProcessors.TryGetValue(rpcID, out checkExist))
            {
                return false;
            }
                
            _rpcProcessors.Add(rpcID, rpcProcessor);
            return true;
        }

        /// <summary>
        /// Unsubscribe from an rpc
        /// </summary>
        /// <param name="rpcID"></param>
        public void UnsubscribeFromRpc(byte rpcID)
        {
            _rpcProcessors.Remove(rpcID);
        }

        internal static void CallRpc(ushort id, byte rpcID, NetMessage message)
        {
            NetworkedSceneView sceneView;
            if (SceneObjects.TryGetValue(id, out sceneView))
            {
                Action<NetMessage> processor;
                if (sceneView._rpcProcessors.TryGetValue(rpcID, out processor))
                {
                    if (processor != null)
                        processor(message);
                    else
                    {
                        Debug.LogWarning("RPC processor for {0} was null. Automatically cleaning up. Please be sure to clean up after yourself in the future.", rpcID);
                        sceneView._rpcProcessors.Remove(rpcID);
                    }
                }
                else
                {
                    Debug.LogWarning("NetworkedSceneView on {0}: unhandled RPC {1}", id, rpcID);
                }
            }
            
        }

        #endregion

        /// <summary>
        /// Send an rpc to the server
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="args"></param>
        public void Rpc(byte rpcID, params object[] args)
        {
            var size = 0;
            foreach (var arg in args)
            {
                if (arg == null)
                    throw new NullReferenceException("Cannot serialize null value");

                size += _net.Serializer.SizeOf(arg);
            }

            var msg = StartMessage(rpcID, RpcMode.ServerOrdered, size);
            foreach (var arg in args)
            {
                _net.Serializer.Serialize(arg, msg);
            }
            ImplSendMessage(msg, ReliabilityMode.Ordered);
        }

        partial void ImplSendMessage(NetMessage msg, ReliabilityMode mode);

        NetMessage StartMessage(byte rpcId, RpcMode mode, int size)
        {
            var msg = _net.Room.GetMessage(size + 5);

            msg.Write(RpcUtils.GetHeader(mode, MsgType.Internal));
            msg.Write(RandPRpcs.SceneObjectRpc);
            msg.Write(NetworkID);
            msg.Write(rpcId);
            return msg;
        }

        /// <summary>
        /// serialize this into a string
        /// </summary>
        /// <returns></returns>
        public string Serialize()
        {
            var sb = new StringBuilder();
            sb.AppendLine("-NetworkedSceneObject-");
            sb.Append("id: ").Append(NetworkID).AppendLine(";");

            return sb.ToString();
        }

        private static readonly Dictionary<ushort, NetworkedSceneView> SceneObjects = new Dictionary<ushort, NetworkedSceneView>();
    }
}