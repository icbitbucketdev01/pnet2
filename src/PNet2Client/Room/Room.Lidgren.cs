﻿#if R_LIDGREN
using PNet;
using Lidgren.Network;

namespace PNetC
{
    public partial class Room
    {
        internal NetConnection Connection { get; set; }

        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _client.RoomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            _client.RoomClient.SendMessage(lmsg, method);
        }

        partial void ImplGetMessage(int size, ref NetMessage message)
        {
            message = NetMessage.GetMessage(size);
        }
    }
}
#endif