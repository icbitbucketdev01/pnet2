﻿using System;
using PNet;

namespace PNetC
{
    public partial class Room : IRpcProvider, IProxySingle<IRoomProxy>
    {
        public ConnectionStatus Status { get; internal set; }
        public string StatusReason { get; set; }
        public Guid RoomId { get; internal set; }
        public float Latency { get; set; }

        private readonly Client _client;
        public Client Client { get { return _client; } }
        internal Room(Client client)
        {
            _client = client;
        }

        private void CallRpc(byte rpcId, NetMessage msg)
        {
            var proc = _rpcProcessors[rpcId];
            if (proc == null)
            {
                Debug.LogWarning("Unhandled static room rpc {0}", rpcId);
            }
            else
                try
                {
                    proc(msg);
                }
                catch (Exception e)
                {
                    Debug.LogException(e, "Tried to call RPC {0}", rpcId);
                }
        }

        readonly Action<NetMessage>[] _rpcProcessors = new Action<NetMessage>[256];

        public bool SubscribeToRpc(byte rpcId, Action<NetMessage> action)
        {
            _rpcProcessors[rpcId] = action;
            return true;
        }

        public void UnsubscribeRpc(byte rpcId)
        {
            _rpcProcessors[rpcId] = null;
        }

        /// <summary>
        /// subscribe all methods marked with the Rpc attribute on the specified object, or any interface implementations that have rpc-marked methods
        /// </summary>
        /// <param name="obj"></param>
        public void SubscribeRpcsOnObject(object obj)
        {
            RpcSubscriber.SubscribeObject<RpcAttribute>(this, obj, _client.Serializer, Debug.Logger);
        }

        public void ClearSubscriptions()
        {
            for (int i = 0; i < _rpcProcessors.Length; i++)
            {
                _rpcProcessors[i] = null;
            }
        }

        internal void SendMessage(NetMessage msg, ReliabilityMode mode)
        {
            ImplementationSendMessage(msg, mode);
        }
        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode);

        private IRoomProxy _proxyObject;
        /// <summary>
        /// the value set from Proxy(IRoomProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>()
        {
            return (T)_proxyObject;
        }
        /// <summary>
        /// set the proxy object to use when returning Proxy`T().
        /// This is only useful if you're using something like Castle.Windsor's dynamic proxy generation.
        /// </summary>
        /// <param name="proxy"></param>
        public void Proxy(IRoomProxy proxy)
        {
            _proxyObject = proxy;
            _proxyObject.Room = this;
        }

        public NetMessage GetMessage(int size)
        {
            NetMessage msg = null;
            ImplGetMessage(size, ref msg);
            return msg;
        }

        partial void ImplGetMessage(int size, ref NetMessage message);
    }
}
