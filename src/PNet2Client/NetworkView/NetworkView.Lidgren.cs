﻿#if R_LIDGREN
using PNet;

namespace PNetC
{
    public partial class NetworkView
    {
        partial void ImplSendMessage(NetMessage msg, ReliabilityMode reliable)
        {
            var lmsg = NetworkManager.Client.RoomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = reliable.PlayerDelivery(out seq);

            NetworkManager.Client.RoomClient.SendMessage(lmsg, method, seq);
        }
    }
}
#endif