﻿using PNet;

namespace PNetC
{
    public partial class Server
    {
        internal void ConsumeData(NetMessage msg)
        {
            if (msg == null) return;
            byte header;
            if (!msg.ReadByte(out header))
                return;
            MsgType msgType;
            BroadcastMode broadcast;
            ReliabilityMode reliability;
            SubMsgType sub;
            RpcUtils.ReadHeader(header, out reliability, out broadcast, out msgType, out sub);

            switch (msgType)
            {
                case MsgType.Internal:
                    ProcessInternal(msg);
                    break;
                case MsgType.Static:
                    ProcessStatic(broadcast, reliability, msg);
                    break;
                default:
                    Debug.LogWarning("Unsupported {0} byte message of type {1} from {2}", msg.LengthBytes - 1, msgType, this);
                    break;
            }
        }

        private void ProcessStatic(BroadcastMode broadcast, ReliabilityMode reliability, NetMessage msg)
        {
            byte rpc;
            if (!msg.ReadByte(out rpc))
                return;
            CallRpc(rpc, msg);
        }

        private void ProcessInternal(NetMessage msg)
        {
            byte id;
            if (!msg.ReadByte(out id))
            {
                Debug.LogError("Malformed internal server rpc");
                return;
            }
            switch (id)
            {
                case DandPRpcs.RoomSwitch:
                    this._client.OnBeginRoomSwitch(msg);
                    break;
            }
        }
    }
}
