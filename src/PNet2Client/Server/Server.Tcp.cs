﻿#if S_TCP
using PNet;
using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PNetC
{
    public partial class Server
    {
        internal TcpClient TcpClient;
        internal NetworkStream NetworkStream;

        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            try
            {
                var strm = NetworkStream;
                if (strm != null)
                    strm.BeginWrite(msg.Data, 0, msg.LengthBytes, Callback, msg);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                TcpClient.Close();
            }
        }

        private void Callback(IAsyncResult ar)
        {
            var msg = ar.AsyncState as NetMessage;
            try
            {
                var strm = NetworkStream;
                if (strm != null)
                    strm.EndWrite(ar);
            }
            catch (ObjectDisposedException ode)
            {
                if (NetworkStream != null && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                {
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    TcpClient.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                TcpClient.Close();
            }
            finally
            {
                NetMessage.RecycleMessage(msg);
            }
            if (!TcpClient.Connected)
                TcpClient.Close();
        }

        partial void ImplGetMessage(int size, ref NetMessage message)
        {
            message = NetMessage.GetMessageSizePad(size);
        }
    }
}
#endif