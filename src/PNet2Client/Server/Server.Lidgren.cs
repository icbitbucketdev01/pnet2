﻿#if S_LIDGREN
using Lidgren.Network;
using PNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PNetC
{
    public partial class Server
    {
        internal NetConnection Connection { get; set; }

        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _client.ServerClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            _client.ServerClient.SendMessage(lmsg, Connection, method);
        }

        partial void ImplGetMessage(int size, ref NetMessage message)
        {
            message = NetMessage.GetMessage(size);
        }
    }
}
#endif