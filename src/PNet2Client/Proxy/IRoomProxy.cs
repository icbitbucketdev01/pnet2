﻿namespace PNetC
{
    public interface IRoomProxy
    {
        Room Room { get; set; }
    }
}
